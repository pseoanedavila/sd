package chat;
import java.net.Socket;
import java.lang.Thread;

public class Client {
	public static void main(String[] args) throws Exception { 
		//Connect server
		Socket server = new Socket("localhost", 9999);
		
		//Create Keyboard Thread
		Thread keyboardThread = new Thread(new KeyboardTask(server.getOutputStream()));
		//Create Screen Thread
		Thread screenThread = new Thread(new ScreenTask(server.getInputStream()));
		
		Thread[] threads = {keyboardThread, screenThread};
		//Run threads
		Utils.startThreads(threads);
		
		//Wait for threads to finish
		Utils.joinThreads(threads);
		
		server.close();
	}
}
