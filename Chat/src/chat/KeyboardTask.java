package chat;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class KeyboardTask implements Runnable {
	//Attributes
	OutputStream out;
	
	//Constructors
	public KeyboardTask(OutputStream out) {
		this.out = out;
	}
	
	//Public methods
	public void run() {
		//Socket output
		OutputStreamWriter socketOSW = new OutputStreamWriter(out);
		BufferedWriter socketBW = new BufferedWriter(socketOSW);
		
		//Keyboard input
		InputStreamReader keyboardISR = new InputStreamReader(System.in);
		BufferedReader keyboardBR = new BufferedReader(keyboardISR);
		
		mainLoop(socketBW, keyboardBR);
	}
	
	//Private methods
	private void mainLoop(BufferedWriter socketBW, BufferedReader keyboardBR) {
		try {
			String line = readLine(keyboardBR);
			while(Utils.isNotEnd(line)) {
				socketBW.write(line);
				socketBW.newLine();
				socketBW.flush();
				line = readLine(keyboardBR);
			}
		} catch (Exception e) {
			System.out.println("An unexpected error ocurred: " + e.getMessage());
		}
	}
	
	private String readLine(BufferedReader br) throws Exception {
		System.out.print("Write a message: ");
		return br.readLine();
	}
}
