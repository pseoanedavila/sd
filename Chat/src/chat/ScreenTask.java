//Prints on screen the received lines until * is received
package chat;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ScreenTask implements Runnable {
	//Attributes
	private InputStream in;
	
	//Constructors
	public ScreenTask(InputStream in) {
		this.in = in;
	}
	
	//Public methods
	public void run() {
		InputStreamReader isr = new InputStreamReader(in); //Converts bytes to chars
		BufferedReader br = new BufferedReader(isr); //Converts chars to lines
		mainLoop(br);
	}
	
	//Private methods
	private void mainLoop(BufferedReader br) {
		try {
			String line = br.readLine();
			while(Utils.isNotEnd(line)) {
				System.out.println();
				System.out.println(currentHour() + ": " + line);
				System.out.print("Write a message: "); //Ater a msg is print, the request to write a new msg is print again.
				line = br.readLine();
			}
		} catch (Exception e) {
			System.out.println("An unexpected error ocurred: " + e.getMessage());
			return;
		}
	}
	
	private String currentHour() {
		Date date = new Date();   // given date
		Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
		calendar.setTime(date);   // assigns calendar to given date 
		String temp = "";
		temp = temp + (new Integer(calendar.get(Calendar.HOUR_OF_DAY)).toString()); 
		temp = temp + (":" + new Integer(calendar.get(Calendar.MINUTE)).toString());
		return temp;
	}
}
