package chat;

import java.net.Socket;
import java.net.ServerSocket;

public class Server {
	public static void main(String[] args) throws Exception {
		ServerSocket server = new ServerSocket(9999);
		Socket client = server.accept();
		
		//Create threads
		Thread keyboardThread = new Thread(new KeyboardTask(client.getOutputStream()));
		Thread screenThread = new Thread(new ScreenTask(client.getInputStream()));
		Thread[] threads = {keyboardThread, screenThread};
		//Start threads
		Utils.startThreads(threads);
		
		//Wait for threads to end
		Utils.joinThreads(threads);
		
		server.close();
	}
}
