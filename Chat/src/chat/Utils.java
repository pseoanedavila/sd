//The purpose of this class is to implement all methods used by
//more than one class, in order to avoid code repetition

package chat;

public class Utils {
	public static Boolean isNotEnd(String line) {
		return !line.equals("*");
	}
	
	public static void startThreads(Thread[] threads) {
		for (Thread t : threads) {
			t.start();
		}
	}
	
	public static void joinThreads(Thread[] threads) throws Exception {
		for (Thread t : threads) {
			t.join();
		}
	}
}
