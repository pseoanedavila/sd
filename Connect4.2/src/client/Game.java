package client;

public class Game {
	//Attributes
	private static Gui gui;
	private static Grid grid;
	private static Connect4 session;
	private static short playerID;
	
	//Main
	public static void main(String[] args) throws Exception {
		//FIXME: If it is not your turn, ask again until it is your turn
		gui = new Gui();
		grid = new Grid();
		session = new Connect4();
		playerID = session.startGame();
		short status;
		while ((status = session.getStatus()) == 2) {
			Thread.sleep(1000);
		}
		boolean isMyTurn = status % 3 == playerID;
		mainLoop(grid, gui, isMyTurn, playerID);
		session.endSession();
	}
	
	private static void mainLoop(Grid grid, Gui gui, boolean isMyTurn, short playerID) throws Exception {
		short status;
		while(true) {
			if (isMyTurn) status = readMove(); 
			else status = readOpponentMove();
			if (status == playerID) {
				gui.printMessage("Congrats player " + playerID + ", you won!!");
				return;
			} else if (status == (playerID + 1) % 2) {
				gui.printMessage("You lost!!. Player " + playerID + " won!");
				return;
			} else if (status == -1) {
				gui.printMessage("There's a draw!");
				return;
			}
			isMyTurn = !isMyTurn;
		}
	}
	
	//Private methods
	private static short readMove() throws Exception {
		short positionChanged = -1;
		short myMove;
		while (true) {
			myMove = gui.getUserInput();
			positionChanged = session.doMove(myMove, playerID);
			if (isValidPosition(positionChanged))
				break;
			switch(positionChanged) {
			case -1:
				gui.printMessage("This column is full, choose another one");
				continue;
			case -2:
				gui.printMessage("Invalid move");
				continue;
			}
		}
		Thread.sleep(1);
		grid.update(positionChanged, true);
		gui.redrawGrid(grid);
		return session.getStatus();
	}
	
	private static short readOpponentMove() throws Exception {
		short temp = session.getMove((short)((playerID + 1) % 2));
		grid.update(temp, false);
		gui.redrawGrid(grid);
		return session.getStatus(); 
	}
	
	private static boolean isValidPosition(int positionChanged) {
		return positionChanged >= 0 && positionChanged <= 41;
	}
	
}

