package client;

public class Grid {
	/*
	 * 0: Empty cell
	 * 1: My chip
	 * 2: My opponent's chip
	 */
  
    private static final short columns = 7; 
    private static final short rows = 6; 
    private static final short size = rows * columns;
    private short[] matrix;

    public Grid() {
        matrix = new short[size];
        for (short i = 0; i < size; i++) {
            matrix[i] = -1;
        }
    }

    public boolean update(short position, boolean isMyChip) {
    	short number = isMyChip ? (short)0 : (short)1;
    	if (position < 0 || position > 41)
    		return false;
    	matrix[position] = number;
    	return true;
    }
    
    public short getElement(short i, short j) {
    	if (i < 0 || i >= rows)
    		return -1;
    	if (j < 0 || j >= columns) 
    		return -1;
    	short absolutePosition = (short)(i * columns + j);
    	return matrix[absolutePosition];
    }
    
}