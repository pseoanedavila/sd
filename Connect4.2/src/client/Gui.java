package client;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class Gui {
    //Attributes

    private JFrame frame;
    private JLabel[][] slots;
    private JButton[] buttons;
    
    private short lastButtonPressed = -1;
    private boolean buttonChanged = false;

    private int xsize = 7; //Columns
    private int ysize = 6; //Rows
    
    //Constructors
    public Gui() {
        frame = new JFrame("Connect4 Online. Waiting for opponent");

        JPanel panel = (JPanel) frame.getContentPane();
        panel.setLayout(new GridLayout(xsize, ysize + 1));
        
        setUpButtons(panel);
        setUpSlots(panel);
        setUpFrame(frame, panel);
    }
    
    //Public methods
    public void redrawGrid(Grid grid) {
    	//JPanel goes by columns, not by rows like normal people who
    	//want an efficient use of the cache memory :(
    	short element;
    	for (short row = 0; row < ysize; row++) {
    		for (short col = 0; col < xsize; col++) {
    			element = grid.getElement(row, col);
    			if (element == 0) {
    				slots[col][row].setBackground(Color.red);
    				slots[col][row].setOpaque(true);
    			}
    			else if (element == 1) {
    				slots[col][row].setBackground(Color.blue);
    				slots[col][row].setOpaque(true);	
    			}
    		}
    	}
    }
    
    public short getUserInput() throws Exception {
    	enableButtons();
    	while (!buttonChanged) {Thread.sleep(200);}
    	buttonChanged = false;
    	return lastButtonPressed;
    }
    
    public void updateFrameName(String name) {
    	frame.setName(name);
    }
    
    public void printMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }
    
    //Private methods
    private void setUpButtons(JPanel panel) {
        buttons = new JButton[xsize];

        for (int i = 0; i < xsize; i++) {
            buttons[i] = new JButton("" + (i + 1));
            buttons[i].setActionCommand("" + i);
            buttons[i].addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            int a = Integer.parseInt(e.getActionCommand());
                            lastButtonPressed = (short)a;
                            buttonChanged = true;
                            disableButtons();
                        }
                    });
            panel.add(buttons[i]);
        }
        disableButtons();
    }
    
    private void disableButtons() {
    	for (JButton b : buttons) {
    		b.setEnabled(false);
    	}
    }
    
    private void enableButtons() {
    	for (JButton b : buttons) {
    		b.setEnabled(true);
    	}
    }
    
    //Setup GUI elements
    private void setUpSlots(JPanel panel) {
        slots = new JLabel[xsize][ysize];
        for (int column = 0; column < ysize; column++) {
            for (int row = 0; row < xsize; row++) {
                slots[row][column] = new JLabel();
                slots[row][column].setHorizontalAlignment(SwingConstants.CENTER);
                slots[row][column].setBorder(new LineBorder(Color.black));
                panel.add(slots[row][column]);
            }
        }
    }
    
    private void setUpFrame(JFrame frame, JPanel panel) {
    	frame.setContentPane(panel);
        frame.setSize(
                700, 600);
        frame.setVisible(
                true);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
