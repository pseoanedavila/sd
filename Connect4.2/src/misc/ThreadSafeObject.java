package misc;

public class ThreadSafeObject<T>  {
	private T value;
	private Object mutex = new Object();
	
	//MARK: Constructors
	public ThreadSafeObject() {}
	
	public ThreadSafeObject(T value) {
		synchronized(mutex) {
			this.value = value;
		}
	}
	
	//MARK: Public
	public T getValue() {
		synchronized(mutex) {
			return value;
		}
	}
	
	public void updateValue(T value) {
		synchronized(mutex) {
			this.value = value;
		}
	}
}
