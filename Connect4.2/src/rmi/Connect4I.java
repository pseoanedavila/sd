package rmi;

public interface Connect4I {
	
	/*
	 * Starts the game immediately if there's another available player or
	 * waits for one if there is not. It returns your playerID (0 or 1)
	 */
	public short startGame() throws Exception;

	/*
	 * Does a move and returns:
	 * -2: ERROR: It's not your turn
	 * -1: ERROR: Column full
	 * [0,41]: The position that changed in the matrix
	 */
	public short doMove(short move, short playerID) throws Exception;

	/*
	 * Returns the position that has changed in the matrix after the players last move
	 */
	public short getMove(short playerID) throws Exception;

	
	/*
	 * -1: Draw
	 * 0: Player 0 won
	 * 1: Player 1 won
	 * 2: Game is not in progress yet
	 * 3: Game is in progress. Player's 0 turn
	 * 4: Game is in progress. Player's 1 turn
	 */
	public short getStatus() throws Exception; 
}


