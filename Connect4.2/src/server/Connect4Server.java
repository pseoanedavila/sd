package server;

import misc.ThreadSafeMatrix;
import misc.ThreadSafeObject;
import rmi.*;

//TODO: Make it thread safe
public class Connect4Server implements Connect4I {
	//Constants
	private static final short COLUMNS = 7;
	private static final short ROWS = 6;
	private static final short MAX = 4;
	
	//Attributes
	private ThreadSafeMatrix grid;
	private ThreadSafeObject<Short> currentTurn, positionsFull;
	public Object threadCondition;
	private ThreadSafeMatrix playerHasWon, playersLastModifiedPosition; //{player0, player1}. 
	
	/*
	 * Absolute position, {player0, player1}
	 * Needed to get the opponent's move
	 */
	
	//Constructors
	public Connect4Server() {
		currentTurn = new ThreadSafeObject<Short>(Short.valueOf((short) -1));
		positionsFull = new ThreadSafeObject<Short>(Short.valueOf((short)0));
		grid = new ThreadSafeMatrix(ROWS, COLUMNS);
		playerHasWon = new ThreadSafeMatrix((short)1,(short)2);
		playersLastModifiedPosition = new ThreadSafeMatrix((short)1,(short)2);
		playerHasWon.setAllValuesTo((short)0);
		playersLastModifiedPosition.setAllValuesTo((short)-1);
		grid.setAllValuesTo((short)-1);
		threadCondition = new Object();
	}
	
	
	//Interface Methods
	
	public short startGame() throws Exception {
		currentTurn.updateValue((short)0);
		return currentTurn.getValue();
	}
	
	public short doMove(short move, short playerID) throws Exception {
		short row;
		if (playerID != currentTurn.getValue()) return -2;
		if ((row = getFreeSpotInColumn(move)) == -1) {
			System.out.println("Sending -1 to "+playerID);
			return -1; //Column full
		}
		short newTurn = (short)((playerID + 1) % 2);
		
		short positionChanged = insertAtPosition((short)playerID, row, move);
		playersLastModifiedPosition.updateAt(positionChanged,(short) 0, playerID);
		currentTurn.updateValue(newTurn);
		positionsFull.updateValue((short)(positionsFull.getValue() + 1));
		return positionChanged;
	}
	
	
	public short getMove(short playerID) throws Exception {
		return playersLastModifiedPosition.elementAt((short)0, playerID);
	}

	public short getStatus() throws Exception {
		if (playerHasWon.elementAt((short)0, (short)0) == 1) return 0;
		if (playerHasWon.elementAt((short)0, (short)1) == 1) return 1;
		if (positionsFull.getValue() == ROWS * COLUMNS) return -1; //Draw
		return (short)(currentTurn.getValue() + 3); //Returns turn
	}
	
	
	//PRECD: The position must be empty
	public short insertAtPosition(short playerID, short x, short y) {
		grid.updateAt(playerID, x, y);
		for (short dx = -1; dx <= 1; dx++) {
			for (short dy = -1; dy <= 1; dy++) {
				if(dx == 0 && dy == 0) continue; //Direction (0,0) is staying in the same place
				if (areFourInDirection(x,y,dx,dy,playerID)) 
					playerHasWon.updateAt((short)1, (short)0, playerID);
			}
		}
		
		return getAbsolutePosition(x,y);
	}
	
	public short getFreeSpotInColumn(short column) {
		for (short x = ROWS - 1 ; x >=0 ; x--) {
			if (grid.elementAt(x, column) == -1) return x;
		}
		return -1;
	}
	
	public short getAbsolutePosition(short x, short y) {
		return (short)(x * COLUMNS + y);
	}
	
	/*
	 * Checks if, given a position and a direction, there are four chips connected
	 */
    private boolean areFourInDirection(short x, short y, short dx, short dy, short player) {
        short count = 0;
        short tempx = x;
        short tempy = y;
        
        //Go forward in the given direction
        while (count < MAX  && valid(tempx, tempy)) {
            if (grid.elementAt(tempx, tempy) != player) {
                break;

            }
            tempx += dx;
            tempy += dy;
            count++;
        }
        return count == MAX;
    }

    private boolean valid(short x, short y) {
        return x >= 0 && x < ROWS && y >= 0 && y < COLUMNS;
    }

}
