package server;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private static Connect4Server game;
	
	public static void main(String[] args) throws Exception {
		ServerSocket server = new ServerSocket(9999);
		try {
			while(true) {
				game = new Connect4Server();
				Socket player1 = server.accept();
				System.out.println("Accepted 1");
				Thread t1 = new Thread(new PlayerThreadTask(player1, game, (short)0));
				t1.start();
				Socket player2 = server.accept();
				System.out.println("Accepted 2");
				Thread t2 = new Thread(new PlayerThreadTask(player2, game, (short)1));
				t2.start();
				game.startGame();
			}
		} finally {
			try {
				server.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
