package client;

public class Client {
	//Attributes
	private static Gui gui;
	private static Grid grid;
	private static ClientRMI session;
	
	//Main
	public static void main(String[] args) throws Exception {
		session = new ClientRMI();
		boolean isMyTurn = session.startGame();
		gui = new Gui();
		grid = new Grid();
		while (true) {
			if (isMyTurn && readMove())
				break;
			if (readOpponentMove())
				break;
			isMyTurn = !isMyTurn;
		}
		session.endSession();
	}
	
	//Private methods
	private static boolean readMove() throws Exception {
		short positionChanged = -1;
		short myMove;
		while (true) {
			myMove = gui.getUserInput();
			positionChanged = session.doMove(myMove);
			if (isValidPosition(positionChanged))
				break;
			switch(positionChanged) {
			case -1:
				gui.printMessage("This column is full, choose another one");
			case -2:
				gui.printMessage("It's not your turn, wait for your opponent to make a move");
			case -4:
				gui.printMessage("The game is not in progress");
			}
		}
		grid.update(positionChanged, true);
		gui.redrawGrid(grid);
		return session.haveWon();
	}
	
	private static boolean readOpponentMove() throws Exception {
		grid.update(session.getMove(), false);
		gui.redrawGrid(grid);
		return session.hasWon();
	}
	
	private static boolean isValidPosition(short positionChanged) {
		return positionChanged >= 0 && positionChanged <= 41;
	}
	
}

