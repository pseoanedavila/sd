package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import rmi.*;

public class ClientRMI implements RMI{
	//Attributes
	private Socket server;
	private OutputStream os;
	private InputStream is;
	private DataOutputStream dos;
	private DataInputStream dis;
	//Constructors
	public ClientRMI() throws Exception {
		try {
			this.server = new Socket("localhost", 9999);
			this.os = this.server.getOutputStream();
			this.is = this.server.getInputStream();
			this.dos = new DataOutputStream(this.os);
			this.dis = new DataInputStream(this.is);
		} catch (Exception e){
			System.out.println("Error while connecting to server");
			throw e;
		}
	}
	
	//Public methods
	public boolean startGame() throws Exception {
		try {
			//Tell the server we want to start a new game
			return receiveBoolean();
		} catch (Exception e) {
			System.out.println("Network error");
			throw e;
		}
	}
	
	public short doMove(short move) throws Exception {
		sendShort(move);
		return receiveShort();
	}
	
	public short getMove() throws Exception {
		sendShort((short)7);
		return receiveShort();
	}
	
	public boolean haveWon() throws Exception {
		sendShort((short)8);
		return receiveBoolean();
	}
	
	public boolean hasWon() throws Exception {
		sendShort((short)9);
		return receiveBoolean();
	}
	
	public void endSession() throws Exception {
		this.server.close();
		this.dis.close();
		this.dos.close();
	}
	
	//Private methods
	
	private void sendShort(short data) throws Exception {
		this.dos.writeShort(data);
		this.dos.flush();
	}
	
	private short receiveShort() throws Exception {
		return this.dis.readShort();
	}
	
	private boolean receiveBoolean() throws Exception {
		return this.dis.readBoolean();
	}
	
}
