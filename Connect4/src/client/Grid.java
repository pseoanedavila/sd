package client;

public class Grid {
	/*
	 * 0: Empty cell
	 * 1: My chip
	 * 2: My opponent's chip
	 */
  
    private static final short xsize = 7;
    private static final short ysize = 6;
    private static final short size = xsize * ysize;
    private short[] matrix;

    public Grid() {
        matrix = new short[size];
        for (int i = 0; i < size; i++) {
            matrix[i] = 0;
        }
    }

    public boolean update(short position, boolean isMyChip) {
    	short number = isMyChip ? (short)1 : (short)2;
    	if (position < 0 || position > 41)
    		return false;
    	matrix[position] = number;
    	return true;
    }
    
    public short getElement(short i, short j) {
    	if (i < 0 || i >= xsize)
    		return -1;
    	if (j < 0 || j >= ysize) 
    		return -1;
    	int absolutePosition = i * ysize + j;
    	return matrix[absolutePosition];
    }
    
}