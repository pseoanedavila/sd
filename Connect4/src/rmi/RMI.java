package rmi;

public interface RMI {
	public boolean startGame() throws Exception;
	/*
	 * Starts the game immediately if there's another available player or
	 * waits for one if there is not. It returns your turn (true or false)
	 */
	
	public short doMove(short move) throws Exception;
	/*
	 * Does a move and returns:
	 * -2: ERROR: It's not your turn
	 * -1: ERROR: Column full
	 * [0,41]: The position that changed in the matrix
	 */
	
	public short getMove() throws Exception;
	/*
	 * Returns the position that has changed in the matrix after your opponent's move
	 */
	
	public boolean haveWon() throws Exception; 
	/*
	 * Returns true if the caller has won
	 */
	
	public boolean hasWon() throws Exception; 
	/*
	 * Returns true if the caller's opponent has won
	 */
}


