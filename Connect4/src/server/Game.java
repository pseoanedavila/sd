package server;

//TODO: Make it thread safe
public class Game {
	//Constants
	private static final short MAX_X = 7;
	private static final short MAX_Y = 6;
	private static final short MAX = 4;
	
	//Attributes
	private short[][] grid;
	private boolean inProgress;
	private short currentTurn; //TODO: Use this crap
	private boolean[] playerHasWon = {false, false}; //{player0, player1}. 
	
	/*
	 * Absolute position, {player0, player1}
	 * Needed to get the opponent's move
	 */
	private short[] playersLastModifiedPosition = {-1, -1};
	
	/*
	 * Needed to wait until the opponent moves
	 */
	private boolean[] playerMovedSinceLastCheck = {false, false}; 
	
	//Constructors
	public Game() {
		currentTurn = 1;
		inProgress = false;
		grid = new short[MAX_X][MAX_Y];
		initializeGrid();
	}
	
	//Public methods
	public void startGame() {
		inProgress = true;
	}
	
	public boolean isInProgress() {
		return inProgress;
	}
	
	public short getCurrentTurn() {
		return currentTurn;
	}
	
	public void insertAtPosition(short playerID, short x, short y) {
		grid[x][y] = playerID;
		
		for (short dx = -1; dx <= 1; dx++) {
			for (short dy = -1; dy <= 1; dy++) {
				if(dx == 0 && dy == 0) continue;
				if (areFourInDirection(x,y,dx,dy,playerID)) {
					playerHasWon[playerID - 1] = true;
					inProgress = false;
					return;
				}
			}
		}
	}
	
	public short getFreeSpotInColumn(short column) {
		for (short x = MAX_Y - 1 ; x >=0 ; x--) {
			if (grid[x][column] == 0) return x;
		}
		return -1;
	}
	
	//Private methods
	private void initializeGrid() {
		for (short i = 0; i < MAX_X; i++) {
			for (short j = 0; j< MAX_Y; j++) {
				grid[i][j] = 0;
			}
		}
	}
	
	public short getAbsolutePosition(short x, short y) {
		return (short)(x * MAX_Y + y);
	}
	
	public boolean hasPlayerWon(short playerID) {
		return playerHasWon[playerID - 1];
	}
	
	/*
	 * Checks if, given a position and a direction, there are four chips connected
	 */
    private boolean areFourInDirection(short x, short y, short dx, short dy, short player) {
        short count = 0;
        short tempx = x;
        short tempy = y;
        
        //Go forward in the given direction
        while (count < MAX  && valid(tempx, tempy)) {
            if (grid[tempx][tempy] != player) {
                break;

            }
            tempx += dx;
            tempy += dy;
            count++;
        }
        return count == MAX;
    }

    private boolean valid(int x, int y) {
        return x >= 0 && x < MAX_X && y >= 0 && y < MAX_Y;
    }
}
