package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

//TODO: Implement the turns system and make things thread safe
public class Player implements Runnable{
	//Attributes
	private Game game;
	private DataOutputStream dos;
	private DataInputStream dis;
	private Socket client;
	private short playerID;
	private boolean playerWon;
	private boolean playerLost;
	
	//Constructors
	public Player(Game game, InputStream is, OutputStream os, short playerID, Socket client) {
		this.game = game;
		this.dis = new DataInputStream(is);
		this.dos = new DataOutputStream(os);
		this.playerID = playerID;
		this.client = client;
	}

	@Override
	public void run() {
		short option;
		//FIXME: Make it with observers
		while (!game.isInProgress()) {}
		try {
			while (true) {
				option = receiveShort();
				processOption(option);
			}
		} catch (Exception e) {
			System.out.println("SERVER: Socket error");
			return;
		}
	}
	
	
	//Private methods
	private void processOption(short option) throws Exception {
		if (option >= 0 && option <= 6) {
			short move = makeMove(option);
			sendShort(move);
		}
		switch(option) {
		case 7:
			//TODO
		case 8:
			sendBoolean(game.hasPlayerWon(playerID));
			break;
		case 9:
			sendBoolean(game.hasPlayerWon(getOpponentID()));
			break;
		}
	}
	
	private short getOpponentID() {
		return (short)(playerID == 1 ? 2 : 1);
	}
	
	private short makeMove(short column) {
		short x;
		if (!game.isInProgress()) return -4;
		if ((x = game.getFreeSpotInColumn(column)) < 0) 
			return -1;
		game.insertAtPosition(playerID, x, column);
		return game.getAbsolutePosition(x, column);
	}
	
	private void sendShort(short data) throws Exception {
		this.dos.writeShort(data);
		this.dos.flush();
	}
	
	private void sendBoolean(boolean data) throws Exception {
		this.dos.writeBoolean(data);
		this.dos.flush();
	}
	
	private short receiveShort() throws Exception {
		return this.dis.readShort();
	}
}
